﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallsUIController : MonoBehaviour
{
    public GameObject panelChoseBall;
    public GameObject player;
    public GameObject[] balls;
    List<BallsUI> noActiveList;
    Player playerScript;

    // coin unlock skin
    public Text coinSkinText;
    public GameObject unlockskinOBJ;

    // coin
    public Text coinText;
    // Start is called before the first frame update
    void Start()
    {
        // coin unlock skin
        coinSkinText.text = GameManager.instant.GetPriceBall().ToString();

        noActiveList = new List<BallsUI>();
        //activeBallList = new List<BallsUI>();
        playerScript = player.GetComponent<Player>();

        // check the price is enough
        checkPriceEnough();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void backBtn()
    {
        playerScript.gameRun = true;
        gameObject.SetActive(false);
        panelChoseBall.SetActive(true);
    }

    public void unlockSkin()
    {
        noActiveBalls();
        int count = noActiveList.Count;
        if (count == 0)
            return;
        int rand = Random.Range(0, count);
        noActiveList[rand].isActive = true;
        noActiveList[rand].GetComponent<Image>().enabled = false;
        noActiveList[rand].GetComponent<Button>().enabled = true;
        noActiveList[rand].gameObject.transform.GetChild(1).GetComponent<Image>().enabled = true;
        GameManager.instant.setActiveBall(noActiveList[rand].GetComponent<BallsUI>().number, 1);
        noActiveList.Clear();
        GameManager.instant.setCoin(GameManager.instant.getCoin() - GameManager.instant.GetPriceBall());
        GameManager.instant.setPriceBall(GameManager.instant.GetPriceBall() + 100);
        coinSkinText.text = GameManager.instant.GetPriceBall().ToString();
        coinText.text = GameManager.instant.getCoin().ToString();
        checkPriceEnough();
    }

    void noActiveBalls()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            if (!balls[i].GetComponent<BallsUI>().isActive)
            {
                noActiveList.Add(balls[i].GetComponent<BallsUI>());
            }
        }
    }

    void checkPriceEnough()
    {
        if (GameManager.instant.getCoin() >= GameManager.instant.GetPriceBall())
        {
            unlockskinOBJ.GetComponent<Button>().enabled = true;
            unlockskinOBJ.GetComponent<Image>().enabled = true;
            unlockskinOBJ.transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
        else
        {
            unlockskinOBJ.GetComponent<Button>().enabled = false;
            unlockskinOBJ.GetComponent<Image>().enabled = false;
            unlockskinOBJ.transform.GetChild(0).GetComponent<Image>().enabled = true;
        }
    }
}
