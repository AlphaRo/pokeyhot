﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectBall : MonoBehaviour
{
    public Material mat;
    int countCubes;
    bool animate;
    float timer;
    bool stopTimer;
    bool hideLine;

    //GameObject Player;
    private void Start()
    {
        countCubes = transform.GetChild(1).childCount;
        //Player = GameObject.FindGameObjectWithTag("player");
    }

    private void Update()
    {
        if (!stopTimer && hideLine)
        {
            timer += Time.deltaTime;
            if (timer >= .7f)
            {
                transform.GetChild(1).gameObject.SetActive(false);
                stopTimer = true;
            }
        }
        
        if(animate)
        {
            Vector3 temp = transform.GetChild(0).transform.localScale;
            temp.z = 13f;
            transform.GetChild(0).transform.localScale = Vector3.Lerp(transform.GetChild(0).transform.localScale, temp, Time.deltaTime * 8f);
            if (transform.GetChild(0).transform.localScale.z >= 12f)
                animate = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            colorCubes();
            StartCoroutine(HideColoeCubes());
        }
        
    }

    void colorCubes()
    {
        for (int i = 0; i < countCubes; i++)
        {
            transform.GetChild(1).GetChild(i).GetComponent<MeshRenderer>().material = mat;
        }
        animate = true;
        hideLine = true;
    }

    IEnumerator HideColoeCubes()
    {
        yield return new WaitForSeconds(.3f);
        transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
    }
}
