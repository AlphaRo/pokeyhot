﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coinScript : MonoBehaviour
{
    public Text coinText;
    public GameObject explosionCoin;
    float speed = 100f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            coinexplosion();
        }
    }

    public void coinexplosion()
    {
        GameManager.instant.setCoin(GameManager.instant.getCoin() + 1);
        coinText.text = GameManager.instant.getCoin().ToString();
        Destroy(Instantiate(explosionCoin, transform.position, explosionCoin.transform.rotation) , 1f);
        Destroy(gameObject);
    }
}
