﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxAnimationBounce : MonoBehaviour
{
    public bool firstTime;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            print("player");
            if (!firstTime)
            {
                firstTime = true;
                collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * 40f;
            }
            else
                collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * 25f;

        }
    }

}
