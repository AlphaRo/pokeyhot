﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAnimation : MonoBehaviour
{
    Vector3 direction;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        direction = Vector3.right;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime , Space.World);

        if (transform.position.x >= 6f)
            direction = Vector3.left;
        if (transform.position.x <= -4f)
            direction = Vector3.right;
    }
}
