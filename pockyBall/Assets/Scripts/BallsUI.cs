﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallsUI : MonoBehaviour
{
    public GameObject[] ballsMesh;
    public bool PricipalBall;
    public bool isActive;
    public int number;
    List<BallsUI> activeBallList;
    public GameObject[] balls;
    // Start is called before the first frame update
    void Start()
    {
        loadActiveBall();
        activeBallList = new List<BallsUI>();
        activeBalls();

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BtnActiveBalls()
    {
        // active ball image
        activeBalls();
        for (int i = 0; i < activeBallList.Count; i++)
        {
            if (activeBallList[i].PricipalBall)
            {
                activeBallList[i].PricipalBall = false;
                activeBallList[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
                GameManager.instant.setPricipalBall(activeBallList[i].number, 0);
            }
        }
        PricipalBall = true;
        transform.GetChild(0).GetComponent<Image>().enabled = true;

        // active ball mesh
        for (int i = 0; i < ballsMesh.Length; i++)
        {
            ballsMesh[i].SetActive(false);
        }
        ballsMesh[number].SetActive(true);

        //save 
        GameManager.instant.setPricipalBall(number, 1);
        print(number);
    }
    void activeBalls()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            if (balls[i].GetComponent<BallsUI>().isActive)
            {
                activeBallList.Add(balls[i].GetComponent<BallsUI>());
            }
        }
        
    }
    

    void loadActiveBall()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            if(GameManager.instant.getActiveBall(i) == 1)
            {
                balls[i].GetComponent<BallsUI>().isActive = true;
                balls[i].GetComponent<Image>().enabled = false;
                balls[i].GetComponent<Button>().enabled = true;
                balls[i].transform.GetChild(1).GetComponent<Image>().enabled = true;
            }
            if (GameManager.instant.getPricipalball(i) == 1)
            {
                balls[i].transform.GetChild(0).GetComponent<Image>().enabled = true;
                balls[i].GetComponent<BallsUI>().PricipalBall = true;
                // active ball mesh
                for (int j = 0; j < ballsMesh.Length; j++)
                {
                    ballsMesh[j].SetActive(false);
                }
                ballsMesh[i].SetActive(true);
            }
        }
    }
    
}
