﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChsetBox : MonoBehaviour
{
    static public ChsetBox instant;
    public GameObject particleCoin;
    private void Awake()
    {
        instant = this;
    }
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void openBox()
    {
        anim.SetBool("chsetbox", true);
        Vector3 temp = transform.position;
        temp.z = -.7f;
        Instantiate(particleCoin, temp, particleCoin.transform.rotation);
    }

    //IEnumerator animation()
    //{
    //    anim.SetBool("chsetbox", true);

    //}
}
