﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassBounce : MonoBehaviour
{
    public GameObject GlassExplosion;
    GameObject Player;
    Player playerScript;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        playerScript = Player.GetComponent<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(!playerScript.activeFire)
                other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.down * 50f;
            Instantiate(GlassExplosion, other.transform.position, GlassExplosion.transform.rotation);
            Destroy(gameObject);
        }
    }
}
