﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronSolidFall : MonoBehaviour
{
    public GameObject player;
    bool Isbreak;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!Isbreak)
        {
            if (player.transform.position.y > transform.position.y + 25f)
            {
                Destroy(gameObject, 4f);

                //childreen
                transform.GetChild(3).GetComponent<BoxCollider>().isTrigger = true;
                transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetChild(1).gameObject.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetChild(2).gameObject.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetChild(3).gameObject.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetChild(0).gameObject.GetComponent<Rigidbody>().velocity = new Vector3(4f, -10f, 0f);
                transform.GetChild(1).gameObject.GetComponent<Rigidbody>().velocity = new Vector3(-4f, -10f, 0f);
                transform.GetChild(2).gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0f, -10f, 0f);
                transform.GetChild(3).gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0f, -10f, 0f);

                Isbreak = true;
            }

        }
    }
}
